# Zerar o Fundo Especial de Financiamento de Campanha (FEFC, ou "Fundão")

* Economia: R$ 1,7 bilhão

O fundo especial de financiamento de campanha ampliado pela [lei 13.487, de 6 de outubro de 2017](http://legis.senado.leg.br/legislacao/ListaTextoSigen.action?norma=26248218&id=26248223&idBinario=26248227&mime=application/rtf), permite que os partidos tenham direito a verbas da União para uso em campanha eleitoral. Neste ano de 2018, será disponibilizado aproximadamente [R$ 1,7 bilhão](http://www.tse.jus.br/imprensa/noticias-tse/2018/Maio/tse-aprova-criterios-para-distribuicao-do-fundo-eleitoral) para as campanhas dos partidos brasileiros.
O dinheiro do contribuinte deve ser empregado para atividades que tragam benefícios ao contribuinte. Este fundo visa benificiar *apenas* os partidos políticos e as pessoas diretamente envolvidades com eles, não trazendo qualquer valor a sociedade e aos cidadãos brasileiros.
Caso pessoas queiram se candidatar, elas que devem ir atrás de patrocinadores ou de utilizar o seu próprio dinheiro, assim como nós brasileiros temos que fazer quando queremos correr atrás de nossos sonhos e vontades próprios.