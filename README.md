# O Governo está precisando de ajuda financeira. Ajude-nos a fazer o governo economizar dinheiro!

* R$ 1,7 bilhão economizado do objetivo de R$ 217 bilhões.

Ao longo dos anos o governo tem se mostrado incapaz de administrar o dinheiro que nós contribuintes entregamos a eles. Apenas ano passado foram *2,17 trilhões de reais* arrecadados e, mesmo assim, eles dizem que ainda falta dinheiro.
Parece que estão faltando economistas descentes para fazer gestão - talvez por falta de dinheiro, ou por falta de gente capacitada (como podemos ver pelos comentárias que aparecem nos telejornais) - e por isso decidimos juntar várias ideias dos verdadeiros economistas, nós brasileiros, para tampar "as perdas" que o nosso governo está sofrendo.
Se você tiver qualquer ideia que possa fazer o governo economizar, mesmo que alguns centavos, chegou a sua hora. Vamos juntar todas as ideias e apresentar para nossos "governantes" para que eles possam dar o rumo que nós queremos ao nosso país.
O objetivo é chegar a R$ 217 bilhões de economia, o equivalente a 10% de todos os impostos. Em contrapartida, pediremos a redução de *todos os impostos 10%*!

*Para detalhes sobre como ajudar, veja na sessão de ajuda.

## O objetivo

Conseguir redução de 10% em todos os impostos. E para conseguir isto, precisamos apresentar um plano que economize R$ 217 bilhões aos cofres públicos (10% do valor da arrecadação do ano passado).

## Como conseguiremos

Todos nós sempre tivemos várias ideias sobre os cortes que o governo poderia fazer para economizar - como cortar auxílios, contratar serviços mais baratos, etc. -, mas nunca tivemos a oportunidade de para sermos ouvidos.
Criaremos um conjunto de documentos com várias ideias de economia que nossos políticos deverão acatar, de um jeito que eles consigam fazer os cortes nos impostos sem reclamar de leis como a de responsabilidade fiscal.
Agora é o momento! A paralisação dos caminhoneiros abriu uma brecha para negociações com o governo com o povo. Então nós, o povo brasileiro, precisamos aproveitar este pequeno momento em que o governo nos escutará para mostrar aquilo que realmente esperamos deles.
Vamos mostrar para eles o que nós realmente queremos e precisamos e ainda ir além. Devemos mostrar para eles que nós sabemos de todos os rombos em contas públicas que estão sendo causados por eles, e que não vamos mais aturar qualquer roubo ou desvio do nosso dinheiro de contribuinte.